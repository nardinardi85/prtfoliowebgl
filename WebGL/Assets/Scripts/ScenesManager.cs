﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{    
    Camera mainCamera;
    float loadingScreenMinDuration = 3;
    [SerializeField]
    private GameObject loadingScreenPanel;

    public static ScenesManager instance;

    public static ScenesManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        mainCamera = Camera.main;
    }

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
    }

    public void SceneChange(int index)
    {
        StartCoroutine(CoLoadSceneAsyncAfterSeconds(0, loadingScreenMinDuration, index));
    }

    public void SceneChange(int index, float delay)
    {
        StartCoroutine(CoLoadSceneAsyncAfterSeconds(delay, loadingScreenMinDuration, index));
    }

    private IEnumerator CoLoadSceneAsyncAfterSeconds(float delay, float time, int index)
    {        
        yield return new WaitForSecondsRealtime(delay);

        if(loadingScreenPanel != null) loadingScreenPanel.SetActive(true);

        yield return new WaitForSecondsRealtime(time);

        var asyncOperation = SceneManager.LoadSceneAsync(index);

        if (SceneManager.GetActiveScene().buildIndex != 0 && asyncOperation.isDone)
        {
            if (loadingScreenPanel != null) loadingScreenPanel.SetActive(false);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}