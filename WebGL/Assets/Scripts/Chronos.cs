﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chronos : MonoBehaviour
{
    [HideInInspector]
    public float timeScaleChanger;
    public void TimeScaleController(float timeScale)
    {
        timeScale = timeScaleChanger;
        Time.timeScale = timeScale;
    }

    public void ChangeTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
    }
}
