﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    
    private Transform cameraTransform;

    private void Awake()
    {
        cameraTransform = Camera.main.transform;
    }
    void Update()
    {
        //just in case i'll need to change camera
        transform.LookAt(cameraTransform);        
    }
}
