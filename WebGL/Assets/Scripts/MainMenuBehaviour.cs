﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuBehaviour : MonoBehaviour
{
    bool isLoading = false;
    // Update is called once per frame
    void Update()
    {
        if(Input.anyKey && !isLoading)
        {
            GoToMainScene();
        }
    }

    public void GoToMainScene()
    {
        isLoading = true;
        ScenesManager.instance.SceneChange(1);        
    }
}
