﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    private Vector3 initialPosition;
    private Quaternion initialRotation;

    public Transform ball;
    public float followOffset = 3.5f;
    // Start is called before the first frame update
    void Awake()
    {
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = initialRotation;
        transform.position = new Vector3(initialPosition.x, initialPosition.y, ball.position.z + followOffset);
        //transform.Translate(initialPosition.x, initialPosition.y, ball.position.z - cameraOffset);
    }
}
